# This profile is auto-generated
Profile: debian/main
Enable-Tags-From-Check:
 apache2,
 application-not-library,
 appstream-metadata,
 automake,
 binaries,
 changes-file,
 conffiles,
 control-files,
 cruft,
 dbus,
 deb-format,
 debhelper,
 debian/changelog,
 debian/control,
 debian/copyright,
 debian/debconf,
 debian/po-debconf,
 debian/readme,
 debian/rules,
 debian/source-dir,
 debian/version-substvars,
 debian/watch,
 duplicate-files,
 elpa,
 fields,
 fields/description,
 fields/standards-version,
 filename-length,
 files,
 gir,
 group-checks,
 huge-usr-share,
 infofiles,
 init.d,
 java,
 lintian,
 manpages,
 md5sums,
 menu-format,
 menus,
 nmu,
 nodejs,
 obsolete-sites,
 ocaml,
 patch-systems,
 pe,
 phppear,
 python,
 scripts,
 shared-libs,
 symlinks,
 systemd,
 testsuite,
 triggers,
 udev,
 upstream-metadata,
 upstream-signing-key,
 usrmerge,
Extends:
 debian/ftp-master-auto-reject

